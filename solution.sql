-- 1. Find all artists that has letter d in its name:
SELECT * FROM artists WHERE name Like "%d%";

-- 2. Find all songs that has a length of less than 350:
SELECT * FROM songs WHERE length < 350;

-- 3. Join the 'albums' and 'songs' table. (Only show the album name, song name, and song length.)
SELECT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;

-- 4. Join the 'artists' and 'albums' tables. (Find all the albums that has a letter a in its name.)
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id AND album_title LIKE "%a%";

-- 5. Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT  * FROM songs ORDER BY song_name DESC LIMIT 4;

-- 6. Join the 'albums' and 'songs' table.(Sort albums from Z-A)
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id
	 ORDER BY albums.album_title DESC;